# tony-stark-icon-man

tony stark static Web server

## 安装

````
npm i -g tony-stark-icon-man
````

## 使用方法

````
tony-stark-icon-man # 把当前文件夹作为静态资源服务器根目录

tony-stark-icon-man -p 8080  #设置一个端口号为8080

tony-stark-icon-man -h localhost  #设置host为localhost

tony-stark-icon-man -d /usr  # 设置根目录为/usr

````


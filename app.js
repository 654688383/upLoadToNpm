//引入http模块
const http = require("http");
//引入chalk模块 这是一个作用在vs code上的颜色工具
const chalk = require("chalk");

const path =require("path");

const fs = require("fs")
const route = require("./hleper/route")
const openUrl = require("./hleper/openUrl");

//引入 配置的变量
const conf=require("./config/defaultConfig");

// 定义 服务类  的一些方法
class Server {
    constructor(config){
        // 把 定义的config 和用户输入的config 进行合并
       this.config = Object.assign({},conf,config)
    }

    // 等待 用户 输入命令 调用
    start(){
        //实例话化服务， req 是客户端的请求，包含各种信息； res是 返回到客户端的内容；
        const server = http.createServer( (req,res) => {
            // 可以拿到用户访问的目标文件的 结构目录， 域名后面那串；
            const url=req.url;
        // 拿到用户 当前访问的文件系统 根目录；process.cwd（）；
        const cwd = this.config.root;
        //  上面这俩一拼接 就是实际的后端文件 所在位置；
        const filePath = path.join(cwd,url);

        // 把config 传进去
        route(res,filePath,this.config);

    })
//监听 3030 端口
        server.listen(3030,() => {
            // 定义输出内容
            const addr = `http://${this.config.hosts}:${this.config.ports}`
            // 利用 打印台输出有颜色的内容
            console.log(`server is running in  ${chalk.blue(addr)}`)
        //在浏览器打开
        openUrl(addr )
    })
    }
}

module.exports = Server;



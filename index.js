
const yargs =require("yargs");

const Server = require("./app");

// argv 是指启动时的所有参数 ， 是一个数组；
// 现在, yargs 的功能就是帮你实现类似上面这种定制化的参数功能.
// 从此一些简单定制参数可以彻底摆脱 config 配置了.
const argv = yargs
    .usage('anywhere [options]') // 一句话说明使用方式
    .option('p',{  // 执行 -p的时候跑的是 3030 端口
        alias:"ports",
        describe:"端口号",
        default:"3030"
    })
    .option('h',{// 执行 -h的时候跑的是 127.0.0.1 本地
        alias:"hosts",
        describe:"host",
        default:"127.0.0.1"
    })
    .option('d',{ // 执行 -d的时候跑的是 拿到的路径是 cwd
        alias:"root",
        describe:"root path",
        default:"process.cwd()"
    })
    .version()
    .alias("v", "version")
    .help() // -help信息  可以带出来
    .argv;

console.log(argv);
// 实例话server
const server = new Server(argv);

// 启动程序
server.start();
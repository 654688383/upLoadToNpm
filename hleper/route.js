const fs = require("fs");

const path= require("path");

const promisify = require("util").promisify;

const  stat =  promisify(fs.stat);

const  readdir =  promisify(fs.readdir);




const mimeType = require("../hleper/mime");

//引用handlebars
const Handlebars = require("handlebars");
//文件操作通通使用绝对路径 除非require；
const tpl = path.join(__dirname,"../templete/dir.tpl.html");
//拿到 Handlebars模版引擎额 依赖资源文件 使用同步操作一次；
const source = fs.readFileSync(tpl);
//使用Handlebars.compile得到处理过的模版； 但是接受一个字符串 ，source是buffer 所以要转格式；
const templete = Handlebars.compile(source.toString());


module.exports = async function (res,filePath,config) {
         res.statusCode=200;

    try{
        const status =await stat(filePath);
        // 使用 mime函数去解析文件的 contenttype；这样就能拿到文件的content-type了；
        const contentType = mimeType(filePath);



        if(status.isFile()){
         //如果返回的类型是文件 那么 就在 页面 使用流 读出来， 不然那个 会很慢  
            res.setHeader("Content-Type",contentType)

         //很慢的 文件 读取方式 不建议
     //    fs.readFile(filePath,(err,data) => {
     //        if(err) throw err;
     //        res.end(data);
     //    })

         //把文件流一点点的读到页面里去； 
         fs.createReadStream(filePath).pipe(res);     
     }else if(status.isDirectory()){
         //  //如果返回的类型是文件夹  那么 就在 页面写出 文件名 ，
            res.setHeader("Content-Type","text/html")
         const files=await readdir(filePath);

            // 使用path的relative方法来获取路径；
            const dir = path.relative(config.root,filePath);

            const data = {
                title:path.basename(filePath),
                files:files,
                dir:dir?`/${dir}`:""
            }
         //使用templete的生成的模版；
         res.end(templete(data));
     }

    }catch(ex){
       //如果报错 证明文件是空，那么就返回404
       res.statusCode=404;
       res.setHeader("Content-Type","text/plain")
       res.end("404"+ex);
       return;
    }

}